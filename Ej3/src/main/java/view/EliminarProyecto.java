package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import controller.CientificoController;
import controller.ProyectoController;

public class EliminarProyecto extends JFrame {

	private JPanel contentPane;
	private JTextField proyectoTexto;
	private ProyectoController proyectoController;

	/**
	 * Launch the application.
	 */
	
	/**
	 * Create the frame.
	 */
	public EliminarProyecto() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 710, 375);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("ID Proyecto:");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel.setBounds(181, 84, 102, 23);
		contentPane.add(lblNewLabel);

		
		JButton eliminarButton = new JButton("Eliminar");
		eliminarButton.setFont(new Font("Tahoma", Font.PLAIN, 20));
		eliminarButton.setBounds(242, 146, 180, 70);
		eliminarButton.addActionListener(eliminar);
		contentPane.add(eliminarButton);
		
		JButton btnNewButton_1 = new JButton("<---");
		btnNewButton_1.setBounds(12, 13, 76, 25);
		btnNewButton_1.addActionListener(eliminar);
		contentPane.add(btnNewButton_1);
		
		proyectoTexto = new JTextField();
		proyectoTexto.setBounds(293, 87, 86, 20);
		contentPane.add(proyectoTexto);
		proyectoTexto.setColumns(10);
	}
	
ActionListener eliminar= new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			JButton aux = (JButton) e.getSource();
			if (aux.getText().equals("Eliminar"))
			{
				if (!proyectoTexto.getText().equals(""))
				{
					int respuesta = JOptionPane.showConfirmDialog(null,
							"Esta seguro de eliminar este proyecto?", "Confirmación",
							JOptionPane.YES_NO_OPTION);
					if (respuesta == JOptionPane.YES_NO_OPTION)
					{
						proyectoController.eliminarProyecto(proyectoTexto.getText());
					}
				}
				else{
					JOptionPane.showMessageDialog(null, "Ingrese un ID de proyecto", "Información",JOptionPane.WARNING_MESSAGE);
				}
			}
			if (aux.getText().equals("<---"))
			{
				proyectoController.mostrarMenuPrincipal();
				dispose();
			} 
			
		}
			
	};

public void setProyectoController(ProyectoController proyectoController) {
	this.proyectoController = proyectoController;
}
	
	

}
