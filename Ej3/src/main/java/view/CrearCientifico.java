package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.CientificoController;
import model.dto.Cientifico;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class CrearCientifico extends JFrame {

	private JPanel contentPane;
	private JTextField dniTexto;
	private JTextField nomApelsTexto;
	private CientificoController cientificoController;
	
	
	
	public CrearCientifico() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 710, 375);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton crearButton = new JButton("Crear");
		crearButton.setFont(new Font("Tahoma", Font.PLAIN, 20));
		crearButton.setBounds(210, 233, 180, 70);
		crearButton.addActionListener(crear);
		contentPane.add(crearButton);
		
		JButton btnNewButton_1 = new JButton("<---");
		btnNewButton_1.setBounds(12, 13, 76, 25);
		btnNewButton_1.addActionListener(crear);
		contentPane.add(btnNewButton_1);
		
		JLabel lblNewLabel = new JLabel("DNI:");
		lblNewLabel.setBounds(113, 77, 46, 14);
		contentPane.add(lblNewLabel);
		
		dniTexto = new JTextField();
		dniTexto.setBounds(151, 74, 86, 20);
		contentPane.add(dniTexto);
		dniTexto.setColumns(10);
		
		nomApelsTexto = new JTextField();
		nomApelsTexto.setColumns(10);
		nomApelsTexto.setBounds(334, 74, 86, 20);
		contentPane.add(nomApelsTexto);
		
		JLabel lblNomapels = new JLabel("NomApels:");
		lblNomapels.setBounds(278, 77, 67, 14);
		contentPane.add(lblNomapels);
	}
	
ActionListener crear = new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			JButton aux = (JButton) e.getSource();
			if (aux.getText().equals("Crear"))
			{
				try {
					Cientifico miCientifico=new Cientifico();
					miCientifico.setDni(dniTexto.getText());
					miCientifico.setNomApels(nomApelsTexto.getText());


					
					cientificoController.registrarCientifico(miCientifico);	
				} catch (Exception ex) {
					JOptionPane.showMessageDialog(null,"Error en el Ingreso de Datos","Error",JOptionPane.ERROR_MESSAGE);
					System.out.println(ex);
				}
			}
			if (aux.getText().equals("<---"))
			{
				cientificoController.mostrarMenuPrincipal();
				dispose();
			} 
			
		}
	};



public void setCientificoController(CientificoController cientificoController) {
	this.cientificoController = cientificoController;
}
	
	

}
