package controller;

import java.util.ArrayList;

import model.dto.Asignado;
import model.service.AsignadoServ;
import view.BuscReemAsignacion;
import view.CrearAsignacion;
import view.EliminarAsignacion;
import view.MenuPrincipal;

public class AsignadoController {

		private AsignadoServ asignacionServ;
		private BuscReemAsignacion buscarReemplazar;
		private CrearAsignacion crear;
		private EliminarAsignacion eliminar;
		private MenuPrincipal principal;
		/**
		 * @return the asignadoServ
		 */
		public AsignadoServ getAsignadoServ() {
			return asignacionServ;
		}
		/**
		 * @param asignadoServ the asignadoServ to set
		 */
		public void setAsignadoServ(AsignadoServ asignadoServ) {
			this.asignacionServ = asignadoServ;
		}
		/**
		 * @return the buscarReemplazar
		 */
		public BuscReemAsignacion getBuscarReemplazar() {
			return buscarReemplazar;
		}
		/**
		 * @param buscarReemplazar the buscarReemplazar to set
		 */
		public void setBuscarReemplazar(BuscReemAsignacion buscarReemplazar) {
			this.buscarReemplazar = buscarReemplazar;
		}
		/**
		 * @return the crear
		 */
		public CrearAsignacion getCrear() {
			return crear;
		}
		/**
		 * @param crear the crear to set
		 */
		public void setCrear(CrearAsignacion crear) {
			this.crear = crear;
		}
		/**
		 * @return the eliminar
		 */
		public EliminarAsignacion getEliminar() {
			return eliminar;
		}
		/**
		 * @param eliminar the eliminar to set
		 */
		public void setEliminar(EliminarAsignacion eliminar) {
			this.eliminar = eliminar;
		}
		/**
		 * @return the principal
		 */
		public MenuPrincipal getPrincipal() {
			return principal;
		}
		/**
		 * @param principal the principal to set
		 */
		public void setPrincipal(MenuPrincipal principal) {
			this.principal = principal;
		}
		
		//Hace visible las vstas de Crear, Buscar y actualizar, Eliminar
		public void mostrarMenuPrincipal() {
			principal.setVisible(true);
		}
		
		public void mostrarVentanaCreacion() {
			crear.setVisible(true);
		}
		public void mostrarVentanaBusActu() {
			buscarReemplazar.setVisible(true);
		}
		public void mostrarVentanaEliminar() {
			eliminar.setVisible(true);
		}

		//Llamadas a los metodos CRUD de la capa service para validar los datos de las vistas
		public void registrarAsignacion(Asignado miAsignacion) {
			asignacionServ.validarRegistro(miAsignacion);
		}
		
		public ArrayList<Asignado> buscarAsignacionDNI(String dni) {
			return asignacionServ.validarConsultaDni(dni);
		}
		
		public ArrayList<Asignado> buscarAsignacionID(String id) {
			return asignacionServ.validarConsultaID(id);
		}
		
		public Asignado buscarAsignacion(Asignado miAsignacion) {
			return asignacionServ.validarConsulta(miAsignacion);
		}
		
		public void modificarAsignacion(Asignado miAsignacion) {
			asignacionServ.validarModificacion(miAsignacion);
		}
		
		public void eliminarAsignacion(Asignado miAsignacion) {
			asignacionServ.validarEliminacion(miAsignacion);
		}
}
