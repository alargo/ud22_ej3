package controller;

import model.dto.Proyecto;
import model.service.ProyectoServ;
import view.BuscReemProyecto;
import view.CrearProyecto;
import view.EliminarProyecto;
import view.MenuPrincipal;

public class ProyectoController {
	
	private ProyectoServ proyectoServ;
	private BuscReemProyecto buscarReemplazar;
	private CrearProyecto crear;
	private EliminarProyecto eliminar;
	private MenuPrincipal principal;
	/**
	 * @return the asignacionServ
	 */
	public ProyectoServ getProyectoServ() {
		return proyectoServ;
	}
	/**
	 * @param asignacionServ the asignacionServ to set
	 */
	public void setProyectoServ(ProyectoServ asignacionServ) {
		this.proyectoServ = asignacionServ;
	}
	/**
	 * @return the buscarReemplazar
	 */
	public BuscReemProyecto getBuscarReemplazar() {
		return buscarReemplazar;
	}
	/**
	 * @param buscarReemplazar the buscarReemplazar to set
	 */
	public void setBuscarReemplazar(BuscReemProyecto buscarReemplazar) {
		this.buscarReemplazar = buscarReemplazar;
	}
	/**
	 * @return the crear
	 */
	public CrearProyecto getCrear() {
		return crear;
	}
	/**
	 * @param crear the crear to set
	 */
	public void setCrear(CrearProyecto crear) {
		this.crear = crear;
	}
	/**
	 * @return the eliminar
	 */
	public EliminarProyecto getEliminar() {
		return eliminar;
	}
	/**
	 * @param eliminar the eliminar to set
	 */
	public void setEliminar(EliminarProyecto eliminar) {
		this.eliminar = eliminar;
	}
	/**
	 * @return the principal
	 */
	public MenuPrincipal getPrincipal() {
		return principal;
	}
	/**
	 * @param principal the principal to set
	 */
	public void setPrincipal(MenuPrincipal principal) {
		this.principal = principal;
	}
	
	//Hace visible las vstas de Crear, Buscar y actualizar, Eliminar
	public void mostrarMenuPrincipal() {
		principal.setVisible(true);
	}
	
	public void mostrarVentanaCreacion() {
		crear.setVisible(true);
	}
	public void mostrarVentanaBusActu() {
		buscarReemplazar.setVisible(true);
	}
	public void mostrarVentanaEliminar() {
		eliminar.setVisible(true);
	}

	//Llamadas a los metodos CRUD de la capa service para validar los datos de las vistas
	public void registrarProyecto(Proyecto miAsignacion) {
		proyectoServ.validarRegistro(miAsignacion);
	}
	
	public Proyecto buscarProyecto(String codigo) {
		return proyectoServ.validarConsulta(codigo);
	}
	
	public void modificarProyecto(Proyecto miAsignacion) {
		proyectoServ.validarModificacion(miAsignacion);
	}
	
	public void eliminarProyecto(String codigo) {
		proyectoServ.validarEliminacion(codigo);
	}
}
