package model.service;

import java.util.ArrayList;

import javax.swing.JOptionPane;

import controller.AsignadoController;
import model.dao.AsignadoDao;
import model.dto.Asignado;

public class AsignadoServ {
	
	private AsignadoController asignacionController;
	public static boolean consultaAsignacion=false;
	public static boolean modificaAsignacion=false;
		
	
	
	/**
	 * @return the asignacionController
	 */
	public AsignadoController getAsignacionController() {
		return asignacionController;
	}

	/**
	 * @param asignacionController the asignacionController to set
	 */
	public void setAsignacionController(AsignadoController asignacionController) {
		this.asignacionController = asignacionController;
	}

	
	public void validarRegistro(Asignado miAsignacion) {
		CientificoServ c= new CientificoServ();
		ProyectoServ p =  new ProyectoServ();
		if(c.existeCientifico(Integer.valueOf(miAsignacion.getCientifico())) && p.existeProyecto(miAsignacion.getProyecto()) && !existeAsignacion(miAsignacion)) {
			AsignadoDao miAsignacionDao = new AsignadoDao();
			miAsignacionDao.registrarAsignacion(miAsignacion);
		}else {
			JOptionPane.showMessageDialog(null,"No existe alguno de los parametros de la asignacion.","Advertencia",JOptionPane.WARNING_MESSAGE);
		}	
		
	}

	public ArrayList<Asignado> validarConsultaDni(String dni) {
		AsignadoDao miAsignacionDao;
		
		try {
			int num = Integer.valueOf(dni);
			if (dni.length() <= 8) {
				miAsignacionDao = new AsignadoDao();
				consultaAsignacion=true;
				return miAsignacionDao.buscarAsignado(null, num, false);					
			}else{
				JOptionPane.showMessageDialog(null,"El dni no puede ser superior a 8 numeros.","Advertencia",JOptionPane.WARNING_MESSAGE);
				consultaAsignacion=false;
			}
			
		}catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(null,"El dni solo debe tener numeros.","Error",JOptionPane.ERROR_MESSAGE);
			consultaAsignacion=false;
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null,"Se ha presentado un Error","Error",JOptionPane.ERROR_MESSAGE);
			consultaAsignacion=false;
		}
					
		return null;
	}
	
	public ArrayList<Asignado> validarConsultaID(String id) {
		AsignadoDao miAsignacionDao;
		
		try {
			if (id.length() <= 4) {
				miAsignacionDao = new AsignadoDao();
				consultaAsignacion=true;
				return miAsignacionDao.buscarAsignado(id, 0, true);					
			}else{
				JOptionPane.showMessageDialog(null,"El id del proyecto no puede ser superior a 4 caracteres.","Advertencia",JOptionPane.WARNING_MESSAGE);
				consultaAsignacion=false;
			}
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null,"Se ha presentado un Error","Error",JOptionPane.ERROR_MESSAGE);
			consultaAsignacion=false;
		}
					
		return null;
	}
	
	public Asignado validarConsulta(Asignado miAsignacion) {
		AsignadoDao miAsignacionDao;
		
		try {
			int num = Integer.valueOf(miAsignacion.getCientifico());
			if (miAsignacion.getProyecto().length() <= 4  && num <= 100000000) {
				miAsignacionDao = new AsignadoDao();
				consultaAsignacion=true;
				return miAsignacionDao.buscarAsignado(miAsignacion);					
			}else{
				JOptionPane.showMessageDialog(null,"El id del proyecto no puede ser superior a 4 caracteres y el dni a 8 numeros.","Advertencia",JOptionPane.WARNING_MESSAGE);
				consultaAsignacion=false;
			}
		}catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(null,"El dni solo debe tener numeros.","Error",JOptionPane.ERROR_MESSAGE);
			consultaAsignacion=false;
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null,"Se ha presentado un Error","Error",JOptionPane.ERROR_MESSAGE);
			consultaAsignacion=false;
		}
					
		return null;
	}

	public void validarModificacion(Asignado miAsignacion) {
		AsignadoDao miAsignacionDao;
        if (existeAsignacion(miAsignacion)) {
            miAsignacionDao = new AsignadoDao();
            miAsignacionDao.modificarAsignado(miAsignacion);
            modificaAsignacion=true;
        }else{
            JOptionPane.showMessageDialog(null,"No existe alguno de los parametros de la asignacion.","Advertencia",JOptionPane.WARNING_MESSAGE);
            modificaAsignacion=false;
        }
		
	}

	public void validarEliminacion(Asignado miAsignacion) {
		AsignadoDao miAsignadoDao=new AsignadoDao();
		if(existeAsignacion(miAsignacion)) { miAsignadoDao.eliminarAsignado(miAsignacion); modificaAsignacion=true;}
		else {modificaAsignacion=false; JOptionPane.showMessageDialog(null,"No existe alguno de los parametros de la asignacion.");}
		
	}

	public boolean existeAsignacion(Asignado miAsignacion) {
		
		AsignadoServ cs = new AsignadoServ();
		
		if (cs.validarConsulta(miAsignacion) != null) {
			return true;
		}
		return false;
	}
}
