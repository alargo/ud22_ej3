package model.service;

import javax.swing.JOptionPane;

import controller.CientificoController;
import model.dao.CientificoDao;
import model.dto.Cientifico;

public class CientificoServ {
	private CientificoController cientificoController;
	public static boolean consultaCientifico=false;
	public static boolean modificaCientifico=false;
	
	/**
	 * @return the cientificoController
	 */
	public CientificoController getCientificoController() {
		return cientificoController;
	}
	/**
	 * @param cientificoController the cientificoController to set
	 */
	public void setCientificoController(CientificoController cientificoController) {
		this.cientificoController = cientificoController;
	}
	
	//Metodo que valida los datos de Registro antes de pasar estos al DAO
	public void validarRegistro(Cientifico miCientifico) {
		
		if(!existeCientifico(Integer.valueOf(miCientifico.getDni()))) {
			CientificoDao miCientificoDao = new CientificoDao();
			miCientificoDao.registrarCientifico(miCientifico);
		}else {
			JOptionPane.showMessageDialog(null,"Ya existe el cientifico con dni: "+miCientifico.getDni(),"Advertencia",JOptionPane.WARNING_MESSAGE);
		}					
	}
	
	
	
	public Cientifico validarConsulta(String codigoCientifico) {
		CientificoDao miCientificoDao;
		
		try {
			int codigo=Integer.parseInt(codigoCientifico);	
			if (codigo > 0) {
				miCientificoDao = new CientificoDao();
				consultaCientifico=true;
				return miCientificoDao.buscarCientifico(codigo);						
			}else{
				JOptionPane.showMessageDialog(null,"El codigo del cientifico no puede estar vacio.","Advertencia",JOptionPane.WARNING_MESSAGE);
				consultaCientifico=false;
			}
			
		}catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(null,"Debe ingresar un dato numerico","Error",JOptionPane.ERROR_MESSAGE);
			consultaCientifico=false;
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null,"Se ha presentado un Error","Error",JOptionPane.ERROR_MESSAGE);
			consultaCientifico=false;
		}
					
		return null;
	}
	
	//Metodo que valida los datos de ModificaciÃ³n antes de pasar estos al DAO
    public void validarModificacion(Cientifico miCientifico) {
        CientificoDao miCientificoDao;
        if (existeCientifico(Integer.valueOf(miCientifico.getDni()))) {
            miCientificoDao = new CientificoDao();
            miCientificoDao.modificarCientifico(miCientifico);
            modificaCientifico=true;
        }else{
            JOptionPane.showMessageDialog(null,"No existe el cientifico con dni: "+miCientifico.getDni(),"Advertencia",JOptionPane.WARNING_MESSAGE);
            modificaCientifico=false;
        }
    }
    
	
    //Metodo que valida los datos de EliminaciÃ³n antes de pasar estos al DAO
	public void validarEliminacion(String codigoCientifico) {
		CientificoDao miCientificoDao=new CientificoDao();
		try {
			int codigo=Integer.parseInt(codigoCientifico);
			miCientificoDao.eliminarCientifico(codigo);
		}catch(NumberFormatException e) {
			JOptionPane.showMessageDialog(null,"Debe ingresar un dato numerico","Error",JOptionPane.ERROR_MESSAGE);
			consultaCientifico=false;
		}
	}
	
	public boolean existeCientifico(int cientifico_dni) {
		
		CientificoServ cs = new CientificoServ();
		
		if (cs.validarConsulta(String.valueOf(cientifico_dni)) != null) {
			return true;
		}
		return false;
	}
	
	
	
}
