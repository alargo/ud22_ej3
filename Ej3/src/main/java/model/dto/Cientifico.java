package model.dto;

public class Cientifico {
	private String dni;
	private String NomApels;
	
	//GETTERS SETTERS
	public String getDni() {
		return dni;
	}
	public void setDni(String dni) {
		this.dni = dni;
	}
	public String getNomApels() {
		return NomApels;
	}
	public void setNomApels(String nomApels) {
		NomApels = nomApels;
	}
	
	
}
