package model.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JOptionPane;

import model.conexion.Conexion;
import model.dto.Cientifico;

public class CientificoDao {

	public void registrarCientifico(Cientifico miCientifico)
	{
		Conexion conex= new Conexion();
		
		try {
			Statement st = conex.getConnection().createStatement();
			String sql= "INSERT INTO Cientifico VALUES ('"+miCientifico.getDni()+"', '"
					+miCientifico.getNomApels()+"');";
			st.executeUpdate(sql);
			JOptionPane.showMessageDialog(null, "Se ha registrado Exitosamente","Información",JOptionPane.INFORMATION_MESSAGE);
			System.out.println(sql);
			st.close();
			conex.desconectar();
			
		} catch (SQLException e) {
            System.out.println(e.getMessage());
			JOptionPane.showMessageDialog(null, "No se Registro");
		}
	}
	
	public Cientifico buscarCientifico(int codigo) 
	{
		Conexion conex= new Conexion();
		Cientifico cientifico= new Cientifico();
		boolean existe=false;
		try {
			String sql= "SELECT * FROM Cientifico where dni = ? ";
			PreparedStatement consulta = conex.getConnection().prepareStatement(sql);
			consulta.setInt(1, codigo);
			ResultSet res = consulta.executeQuery();
			while(res.next()){
				existe=true;
				cientifico.setDni(res.getString("dni"));
				cientifico.setNomApels(res.getString("nomApels"));
			 }
			res.close();
			conex.desconectar();
			System.out.println(sql);
					
			} catch (SQLException e) {
					JOptionPane.showMessageDialog(null, "Error, no se conecto");
					System.out.println(e);
			}
		
			if (existe) {
				return cientifico;
			}
			else return null;				
	}
	
	public void modificarCientifico(Cientifico miCientifico) {
		
		Conexion conex= new Conexion();
		try{
			String consulta="UPDATE Cientifico SET dni= ? ,nomApels = ?  WHERE dni = ? ";
			PreparedStatement estatuto = conex.getConnection().prepareStatement(consulta);
			
            estatuto.setString(1, miCientifico.getDni());
            estatuto.setString(2, miCientifico.getNomApels());
            estatuto.setString(3, miCientifico.getDni());
            estatuto.executeUpdate();
            
          JOptionPane.showMessageDialog(null, " Se ha Modificado Correctamente ","Confirmación",JOptionPane.INFORMATION_MESSAGE);
          System.out.println(consulta);
         

        }catch(SQLException	 e){

            System.out.println(e);
            JOptionPane.showMessageDialog(null, "Error al Modificar","Error",JOptionPane.ERROR_MESSAGE);

        }
	}
	
	public void eliminarCientifico(int codigo)
	{
		Conexion conex= new Conexion();
		try {
			String sql= "DELETE FROM Cientifico WHERE dni = ? ";
			PreparedStatement consulta = conex.getConnection().prepareStatement(sql);
			consulta.setInt(1,codigo);
			int error=consulta.executeUpdate();
			if(error!=0) {
	            JOptionPane.showMessageDialog(null, " Se ha Eliminado Correctamente","Información",JOptionPane.INFORMATION_MESSAGE);
	            System.out.println(sql);
			}else JOptionPane.showMessageDialog(null, "El codigo no existe para ningun cliente."); 
			conex.desconectar();
			
			
		} catch (SQLException e) {
            System.out.println(e.getMessage());
			JOptionPane.showMessageDialog(null, "No se Elimino");
		}
	}
}
